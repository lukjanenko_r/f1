package ru.java2e;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;

public class Player{
	
	Image imgC = new ImageIcon(getClass().getClassLoader().getResource("res/Player.png")).getImage();
	Image imgLeft = new ImageIcon(getClass().getClassLoader().getResource("res/Player_L.png")).getImage();
	Image imgRight = new ImageIcon(getClass().getClassLoader().getResource("res/Player_R.png")).getImage();
	Image imgSky = new ImageIcon(getClass().getClassLoader().getResource("res/Sky.png")).getImage();
	
	Image imgRocket = new ImageIcon(getClass().getClassLoader().getResource("res/Player_Rockets.png")).getImage();
	Image imgRocketL = new ImageIcon(getClass().getClassLoader().getResource("res/Player_L_Rockets.png")).getImage();
	Image imgRocketR = new ImageIcon(getClass().getClassLoader().getResource("res/Player_R_Rockets.png")).getImage();
	
	Image img = imgC;
	Image imgSpeedometer = new ImageIcon(getClass().getClassLoader().getResource("res/Speedometer.png")).getImage();
	
	Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
	
	int v = 1;
	int dv = 0;
	int s = 0;
	
	int destroyedEnemeis = 0;
	int maxSpeed = 0;
	
	int x = (imgSky.getWidth(null) - img.getWidth(null)) / 2;
	int y = (int)screenSize.getHeight() - img.getHeight(null) - 240;
	int dx = 0;
	
	public final int MAX_V = 100;
	public final int MAX_L = 20;
	public final int MAX_R = x * 2 - 35;
	
	int layer1 = 0;
	int layer2 = -imgSky.getHeight(null);
	
	int rockets = 0;
	boolean isRocket = true;
	
	public void startAgain() {
		v = 1;
		dv = 0;
		s = 0;
		dx = 0;
		rockets = 0;
		img = imgC;
		
		destroyedEnemeis = 0;
		maxSpeed = 0;
		
		x = (imgSky.getWidth(null) - img.getWidth(null)) / 2;
		y = (int)screenSize.getHeight() - img.getHeight(null) - 240;
	}
	
	public Rectangle getRect() {
		return new Rectangle(x, y, img.getWidth(null), img.getHeight(null));
	}
	
	public void move(){
		s += v;
		v += dv;
		if (v <= 1) {
			v = 1;
		}
		if (v >= MAX_V) {
			v = MAX_V;
		}
		if (x <= MAX_L) {
			x = MAX_L;
		}
		if (x >= MAX_R) {
			x = MAX_R;
		}
		x += dx;
		if (layer2 + v >= 0) {
			layer2 -= imgSky.getHeight(null) - v;
			layer1 -= imgSky.getHeight(null) - v;
		} else {
			layer1 += v;
			layer2 += v;
			v += dv;
		}
	}
	
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_UP) {
			dv = 2;
		}
		if (key == KeyEvent.VK_DOWN) {
			dv = -1;
		}
		
		if (key == KeyEvent.VK_RIGHT) {
			if (rockets == 0) img = imgRight;
			else img = imgRocketR;
			dx = 20;
		}
		if (key == KeyEvent.VK_LEFT) {
			if (rockets == 0) img = imgLeft;
			else img = imgRocketL;
			dx = -20;
		}
	}

	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_UP || key == KeyEvent.VK_DOWN) {
			dv = 0;
		}
		if (key == KeyEvent.VK_RIGHT || key == KeyEvent.VK_LEFT) {
			if (rockets == 0) img = imgC;
			else img = imgRocket;
			dx = 0;
		}
	}
}
