package ru.java2e;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

public class Main {

	public static void main(String[] args) {
		
		Sky sky = new Sky();
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		
		JFrame f = new JFrame("F1 Java");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.add(sky);
		
		sky.windowSize = (int)screenSize.getHeight() - 55;
		
		f.setSize(sky.img.getWidth(null) + 15, sky.windowSize + 15);
		f.setVisible(true);
		
	}

}
