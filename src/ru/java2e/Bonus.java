package ru.java2e;

import java.awt.Image;
import java.awt.Rectangle;
import java.util.Random;

import javax.swing.ImageIcon;

public class Bonus {
	
	Image imgBomb = new ImageIcon(getClass().getClassLoader().getResource("res/BonusBomb.png")).getImage();
	Image imgRocket = new ImageIcon(getClass().getClassLoader().getResource("res/BonusRocket.png")).getImage();
	
	Image img;
	
	int x;
	int y;
	int v;
	Sky sky;
	
	boolean isBomb = false;
	
	public Rectangle getRect() {
		return new Rectangle(x, y, img.getWidth(null), img.getHeight(null));
	}
	
	public Bonus(int x, int y, int v, Sky sky){
		this.x = x;
		this.y = y;
		this.v = v;
		this.sky = sky;
		
		Random rand = new Random();
		if (rand.nextInt(100) >= 30) {
			img = imgRocket;
		}
		else {
			img = imgBomb;
			isBomb = true;
		}
	}
	
	public void move() {
		y = y + sky.p.v - v;
	}
	
}
