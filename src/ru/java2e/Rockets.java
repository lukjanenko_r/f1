package ru.java2e;

import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

public class Rockets {
	
	Image img = new ImageIcon(getClass().getClassLoader().getResource("res/Rockets.png")).getImage();
	
	int x;
	int y;
	int v;
	Sky sky;
	
	public Rockets(int x, int y, int v, Sky sky){
		this.x = x;
		this.y = y;
		this.v = v;
		this.sky = sky;
	}
	
	public void move() {
		y = y + sky.p.v - v;
	}
	
	public Rectangle getRect() {
		return new Rectangle(x, y, img.getWidth(null), img.getHeight(null));
	}
}
