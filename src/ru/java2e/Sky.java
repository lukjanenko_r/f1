package ru.java2e;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Sky extends JPanel implements ActionListener, Runnable {
	
	Timer mainTimer = new Timer(20, this);
	Image img = new ImageIcon(getClass().getClassLoader().getResource("res/Sky.png")).getImage();
	
	Image imgInstruction = new ImageIcon(getClass().getClassLoader().getResource("res/Instruction.png")).getImage();
	
	Player p = new Player();
	
	int windowSize;
	
	Thread enemiesBonusFactory = new Thread(this);
	List<Enemy> enemies = new ArrayList<Enemy>();
	List<Bonus> bonuses = new ArrayList<Bonus>();
	Rockets r1 = new Rockets(-5000, 0, 0, this);
	Rockets r;
	
	public Sky(){
		r = r1;
		mainTimer.start();
		enemiesBonusFactory.start();
		addKeyListener(new MyKeyAdapter());
		setFocusable(true);
	}
	
	private class MyKeyAdapter extends KeyAdapter {
		public void keyPressed(KeyEvent e) {
			p.keyPressed(e);
			
			int key = e.getKeyCode();
			if (key == KeyEvent.VK_SPACE && p.isRocket == true) {
				if (p.rockets > 0) {
					shoot();
				}
			}
		}
		public void keyReleased(KeyEvent e) {
			p.keyReleased(e);
		}
	}
	
	public void shoot() {
		p.rockets--;
		p.isRocket = false;
		
		if (p.rockets == 0) p.img = p.imgC;
		Rockets rocket = new Rockets(p.x, p.y - p.img.getHeight(null) / 2, p.MAX_V + 3, this);
		r = rocket;
	}
	
	public void paint(Graphics g){
		g = (Graphics2D) g;
		g.drawImage(img, 0, p.layer1, null);
		g.drawImage(img, 0, p.layer2, null);
		
		Iterator<Enemy> i = enemies.iterator();
		while (i.hasNext()){
			Enemy e = i.next();
			if (e.y >= windowSize * 1.2 || e.y <= -windowSize * 1.2) {
				i.remove();
			} else {
				g.drawImage(e.img, e.x, e.y, null);
			}
		}
		
		g.drawImage(p.imgSpeedometer, 0, Math.round(windowSize - p.imgSpeedometer.getHeight(null)) - 25, null);
		
		g.setColor(Color.RED);
		g.translate(p.imgSpeedometer.getWidth(null) / 2, windowSize - 45);
		
		double angle = p.v * Math.PI / p.MAX_V;
		((Graphics2D) g).rotate(angle);
		
		g.drawLine(20, 0, -p.imgSpeedometer.getWidth(null) / 2 + 10, 0);
		
		((Graphics2D) g).rotate(-angle);
		g.translate(-p.imgSpeedometer.getWidth(null) / 2, -windowSize + 45);
		
		g.setColor(Color.GREEN);
		Font font = new Font("Arial", Font.BOLD, 16);
		g.setFont(font);
		g.drawString("Passed (m): " + p.s / 40, p.imgSpeedometer.getWidth(null) / 3, windowSize - p.imgSpeedometer.getHeight(null) / 2);
		
		Iterator<Bonus> j = bonuses.iterator();
		while (j.hasNext()){
			Bonus b = j.next();
			if (b.y >= windowSize * 1.2 || b.y <= -windowSize * 1.2) {
				j.remove();
			} else {
				g.drawImage(b.img, b.x, b.y, null);
			}
		}
		
		g.drawImage(imgInstruction,
				img.getWidth(null) - imgInstruction.getWidth(null),
				windowSize - imgInstruction.getHeight(null) - 25,
				null);
		
		if (p.isRocket == false) g.setColor(Color.BLACK);
		else g.setColor(Color.RED);
		font = new Font("Arial", Font.BOLD, 28);
		g.setFont(font);
		g.drawString("" + p.rockets * 2,
				img.getWidth(null) - imgInstruction.getWidth(null) / 2,
				windowSize - imgInstruction.getHeight(null) * 9 / 10 - 5);
		
		try {
			if (r.y <= -windowSize * 1.2) {
				r = r1;
			} else {
				g.drawImage(r.img, r.x, r.y, null);
			}
		}
		catch (NullPointerException e) {}
		
		g.drawImage(p.img, p.x, p.y, null);
	}
	
	public void actionPerformed(ActionEvent e){
		
		p.move();
		if (p.maxSpeed<p.v) p.maxSpeed = p.v;
		
		try {
			r.move();
		}
		catch (NullPointerException q) {}
		
		Iterator<Enemy> i = enemies.iterator();
		while (i.hasNext()){
			Enemy em = i.next();
			em.move();
			//if (em.isRemove) i.remove();
		}
		
		Iterator<Bonus> j = bonuses.iterator();
		while (j.hasNext()){
			Bonus b = j.next();
			b.move();
		}
		
		repaint();
		
		testCollitionRocketsWithEnemies();
		testCollitionWithEnemies();
		testCollitionWithBonuses();
		
		if (p.s >= 200000){
			int opcion = JOptionPane.showConfirmDialog (this,
					"You have won! 5000 meters passed! \n" + "\nDestroyed enemies: " + p.destroyedEnemeis + "\nMax speed: " + p.maxSpeed * 900 / p.MAX_V + ".\n\nContinue game?",
					"You have won!",
					JOptionPane.YES_NO_OPTION);
			if(opcion == 0){
				enemies.clear();
				bonuses.clear();
				p.startAgain();
			}
			else {
				System.exit(1);
			}
		}
	}

	private void testCollitionWithEnemies() {
		Iterator<Enemy> i = enemies.iterator();
		while (i.hasNext()){
			try{
				Enemy e = i.next();
				if (p.getRect().intersects(e.getRect())) {
					int opcion = JOptionPane.showConfirmDialog (this,
							"You have lost! \n" + "\nDestroyed enemies: " + p.destroyedEnemeis + "\nMax speed: " + p.maxSpeed * 900 / p.MAX_V + ".\n\nContinue game?",
							"You have lost!",
							JOptionPane.YES_NO_OPTION);
					if(opcion == 0){
						enemies.clear();
						bonuses.clear();
						p.startAgain();
						break;
					}
					else {
						System.exit(1);
					}
				}
			}
			catch (RuntimeException e){}
		}
	}
	
	private void testCollitionRocketsWithEnemies() {
		Iterator<Enemy> i = enemies.iterator();
		while (i.hasNext()){
			try{
				Enemy e = i.next();
				if (r.getRect().intersects(e.getRect())) {
					e.crash();
					e.isRemove[0] = true;
					r = r1;
				}
			}
			catch (RuntimeException e){}
		}
	}
	
	private void testCollitionWithBonuses() {
		Iterator<Bonus> i = bonuses.iterator();
		while (i.hasNext()){
			Bonus e = i.next();
			if (p.getRect().intersects(e.getRect())) {
				if (e.isBomb) {
					//enemies.clear();
					Iterator<Enemy> j = enemies.iterator();
					while (j.hasNext()){
						try{
							Enemy em = j.next();
							em.crash();
							em.isRemove[0] = true;
						}
						catch (RuntimeException t){}
					}
					i.remove();
				}
				else {
					p.rockets++;
					i.remove();
					if (p.rockets !=0) p.img = p.imgRocket;
				}
			}
		}
	}
	
	@Override
	public void run() {
		while(true) {
			Random rand = new Random();
			try {
				Thread.sleep(rand.nextInt(1000));
				enemies.add(new Enemy(rand.nextInt(p.MAX_R),
						-windowSize + 1, 
						rand.nextInt(p.MAX_V - p.MAX_V / 5) + p.MAX_V / 7,
						this));
				if (rand.nextInt(100) < 40) {
					bonuses.add(new Bonus(rand.nextInt(p.MAX_R),
							-windowSize + 1, 
							rand.nextInt(p.MAX_V - 30),
							this));
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			try {
				Thread.sleep(1000);
				if (r.x == -5000) p.isRocket = true;
			}
			catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			Iterator<Enemy> i = enemies.iterator();
			while (i.hasNext()){
				try{
					Enemy e = i.next();
					if (e.isRemove[0]) {
						e.isRemoveIndex++;
						if (e.isRemoveIndex <= e.isRemove.length) e.isRemove[e.isRemoveIndex] = true;
					}
					if (e.isRemove[Math.min(e.isRemoveIndex, e.isRemove.length)]) {
						i.remove();
						p.destroyedEnemeis++;
					}
				}
				catch (RuntimeException e){}
			}
		}
	}
}
