package ru.java2e;

import java.awt.Image;
import java.awt.Rectangle;
import java.util.Random;

import javax.swing.ImageIcon;

public class Enemy {
	
	int x;
	int y;
	int v;
	Image imgU = new ImageIcon(getClass().getClassLoader().getResource("res/Enemy.png")).getImage();
	Image imgRocket = new ImageIcon(getClass().getClassLoader().getResource("res/Enemy_Rockets.png")).getImage();
	Image imgC = new ImageIcon(getClass().getClassLoader().getResource("res/AnimationEnemy.gif")).getImage();
	Sky sky;
	
	Image img = imgU;
	
	boolean[] isRemove = new boolean[10];
	int isRemoveIndex = 0;
	
	public Rectangle getRect() {
		return new Rectangle(x, y, img.getWidth(null), img.getHeight(null));
	}
	
	public Enemy(int x, int y, int v, Sky sky){
		this.x = x;
		this.y = y;
		this.v = v;
		this.sky = sky;
		
		Random rand = new Random();
		if (rand.nextInt(2) == 0) img = imgRocket;
	}
	
	public void crash() {
		img = imgC;
	}
	
	public void move() {
		y = y + sky.p.v - v;
	}
}
